const router = require('express').Router();
const templateRoutes = require('./templates.router');

router.use('/templates', templateRoutes);

module.exports = router;
