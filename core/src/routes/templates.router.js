const router = require('express').Router();
const config = require('../config/vars');
const uploadFile = require('multer')({dest: config.uploadsPath});
const templatesController = require('../controllers/templates.controller');

router.post(
    '/upload-template',
    uploadFile.single('template'),
    templatesController.uploadTemplate
)

module.exports = router;
