const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const cors = require('cors');
const logger = require('morgan');
const indexRouter = require('./routes');

const config = require('./config/vars');
const app = express();

app.use(cors({
    origin: config.origin,
    credentials: true
}));

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../public')));
app.use('/api', indexRouter);

const setHeaders = res => {
    res.set('Access-Control-Allow-Origin', '*');
}
app.use('/uploads', express.static(config.uploadsPath, {setHeaders}));

module.exports = app;
