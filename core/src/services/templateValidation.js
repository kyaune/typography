const {runCommand} = require('./runCommand');
const {readFile} = require('fs').promises;

const MM_PER_INCH = 25.4;
const EPSILON = 1;
const MARGIN = 4;
const MIN_DPI = 300;

const validateText = async path => {
    const outputPath = `${path}.text`;
    await runCommand({
        command: 'pdftotext',
        options: [
            path,
            outputPath
        ]
    });
    const file = await readFile(outputPath);
    return !/\S/ig.test(file.toString());
}

const validateColorModel = async path => {
    let isValid = true;
    await runCommand({
        command: 'identify',
        options: [
            '-format',
            '\"%r\"',
            path
        ],
        onStdout: data => {
            if (data.toString().includes('RGB')) isValid = false;
        }
    });
    return isValid;
}

const validatePrintSize = async (path, width, heightSet) => {
    let isWidthValid = true;
    let isHeightValid = true;
    await runCommand({
        command: 'identify',
        options: [
            '-format',
            '\"%x,%y,%U,%w,%h\"',
            path
        ],
        onStdout: data => data
            .toString()
            .split('\"\"')
            .map(str => str.replace(/"/ig, ''))
            .forEach((str, index) => {
                const [xRes, yRes, units, pageWidth, pageHeight] = str.split(',');
                //Assuming that resolution is dpi
                const widthInches = pageWidth / xRes;
                const heightInches = pageHeight / yRes;
                const widthMill = widthInches * MM_PER_INCH;
                const heightMill = heightInches * MM_PER_INCH;
                isWidthValid = widthMill + EPSILON > width + MARGIN;
                isHeightValid = heightMill + EPSILON> heightSet[index] + MARGIN;
            }),
    });
    return isWidthValid && isHeightValid;
}

const validateImagesDPI = async path => {
    let areImagesValid = true;
    await runCommand({
        command: 'pdfimages',
        options: [
            '-list',
            path
        ],
        onStdout: data => data
            .toString()
            .split('\n')
            .slice(2)
            .filter(s => s)
            .map(s => s.split(' ').filter(s => s))
            .filter(imageData => imageData[2] === 'image')
            .forEach(imageData => {
                const dpiX = imageData[12];
                const dpiY = imageData[13];
                areImagesValid = dpiX >= MIN_DPI && dpiY >= MIN_DPI;
            }),
    });
    return areImagesValid;
}

const validateNumberOfPages = async (path, numberOfPages) => {
    let pages = 0;
    await runCommand({
        command: 'identify',
        options: [
            path,
        ],
        onStdout: data => {
            pages += data.toString().split(/\n/ig).filter(t => !!t).length;
        }
    });
    return pages === numberOfPages;
}

module.exports = {
    validateText,
    validateColorModel,
    validatePrintSize,
    validateImagesDPI,
    validateNumberOfPages,
}
