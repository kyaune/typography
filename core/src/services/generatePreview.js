const {runCommand} = require('./runCommand');
const {mkdir, readdir, copyFile} = require('fs').promises;
const path = require('path');
const {getImageWidth, getImageHeight} = require('./getImageSize');
const {TEMPLATE_TOTAL_HEIGHTS, TEMPLATE_SIZE_TO_WIDTH, TEMPLATE_HEIGHT_SETS} = require('../constants/TEMPLATE_PARAMS');
const sleep = require('./sleep');

const assets = {
    mono: [
        path.resolve('./assets', 'mono-1.png'),
    ],
    trio: [
        path.resolve('./assets', 'trio-1.png'),
        path.resolve('./assets', 'trio-2.png'),
        path.resolve('./assets', 'trio-3.png'),
    ]
}

//Generating preview
//1. pdftoppm to slice uploaded template into png pages
//2. identify to get pages width in px
//3. convert -resize to resize asset image to width of page
//4. composite to merge resized asset image to page image
//5. draw shade line 1cm below calendar on each page
//6. convert -append to merge pages in to a single image
//7. on final image draw red line with 4mm offset from border
const generatePreview = async (filePath, size) => {
    const {name: fileName} = path.parse(filePath);
    const dirPath = `${filePath}-preview`;
    await mkdir(dirPath);
    const pagesRoot = path.resolve(dirPath, 'page');
    await runCommand({
        command: 'pdftoppm',
        options: [
            filePath,
            '-png',
            pagesRoot
        ]
    });
    const pages = await readdir(dirPath);
    const pagesNumber = pages.length;
    const assetsSet = pagesNumber === 2 ? assets.mono : assets.trio;
    await Promise.all(assetsSet.map(async (assetPath, index) => {
        const pagePath = path.resolve(dirPath, pages[index + 1]);
        const pageWidth = await getImageWidth(pagePath);
        if (!pageWidth || isNaN(pageWidth)) throw new Error('Something went wrong');
        const resizedAssetPath = path.resolve(dirPath, `asset-${index}.png`);
        await runCommand({
            command: 'convert',
            options: [
                '-resize',
                pageWidth,
                assetPath,
                resizedAssetPath
            ]
        });
        await runCommand({
            command: 'composite',
            options: [
                '-gravity',
                'North',
                resizedAssetPath,
                pagePath,
                pagePath,
            ]
        });
        const pageHeight = await getImageHeight(pagePath);
        const resizedAssetHeight = await getImageHeight(resizedAssetPath);
        const templatePageHeight = TEMPLATE_HEIGHT_SETS[size][index + 1];
        const calendarPageRatio = resizedAssetHeight / pageHeight;
        const pxPerMmY = pageHeight / templatePageHeight;
        const shadeLineOffset = pxPerMmY * 10 + calendarPageRatio * pageHeight;
        await runCommand({
            command: 'convert',
            options: [
                pagePath,
                '-strokewidth',
                '4',
                '-stroke',
                'rgb(87, 255, 132)',
                '-draw',
                `stroke-dasharray 10 10 path \'M 0,${shadeLineOffset.toFixed(0)} L ${pageWidth},${shadeLineOffset.toFixed(0)}\'`,
                pagePath
            ],
        })
    }));
    const resultName = `${fileName}-result.png`;
    const resultPath = path.resolve(dirPath, resultName);
    await runCommand({
        command: 'convert',
        options: [
            '-append',
            path.resolve(dirPath, `page-*`),
            resultPath,
        ]
    });
    await sleep(1000);
    //Draw red line at 4mm offset from border
    const resultWidth = await getImageWidth(resultPath);
    const resultHeight = await getImageHeight(resultPath);
    const templateWidth = TEMPLATE_SIZE_TO_WIDTH[size] + 4 * 2;
    const templateHeight = TEMPLATE_TOTAL_HEIGHTS[size] + 4 * 2;
    const pxPerMmX = resultWidth / templateWidth;
    const pxPerMmY = resultHeight / templateHeight;
    const offsetX = pxPerMmX + 6;
    const offsetY = pxPerMmY + 6;
    const ptsSet = [
        [offsetX.toFixed(0), offsetY.toFixed(0), (resultWidth-offsetX).toFixed(0), offsetY.toFixed(0)],
        [offsetX.toFixed(0), offsetY.toFixed(0), offsetX.toFixed(0), (resultHeight-offsetY).toFixed(0)],
        [(resultWidth-offsetX).toFixed(0), offsetY.toFixed(0), (resultWidth-offsetX).toFixed(0), (resultHeight-offsetY).toFixed(0)],
        [offsetY.toFixed(0), (resultHeight-offsetY).toFixed(0), (resultWidth-offsetX).toFixed(0), (resultHeight-offsetY).toFixed(0)],
    ]
    const options = [
        resultPath,
        '-strokewidth',
        '4',
        '-stroke',
        'red',
    ];
    for (let set of ptsSet) {
        options.push('-draw');
        options.push(`stroke-dasharray 10 10 path \'M ${set[0]},${set[1]} L ${set[2]},${set[3]}\'`);
    }
    options.push(resultPath);
    await runCommand({
        command: 'convert',
        options,
    })

    const publicPath = path.resolve('./public', resultName);
    await copyFile(resultPath, publicPath);
    return resultName;
}

module.exports = {
    generatePreview,
}
