const {runCommand} = require('./runCommand');

const aiToPdf = async file => {
    await runCommand({
        command: 'convert',
        options: [
            file.path,
            `${file.path}.pdf`,
        ],
    });
    return `${file.path}.pdf`;
}

const toPdf = async file => {
    if (file.mimetype === 'application/pdf') return file.path;
    if (file.mimetype === 'application/postscript') return aiToPdf(file);
    throw new Error('Формат файла не поддерживается');
}

module.exports = {
    toPdf
}
