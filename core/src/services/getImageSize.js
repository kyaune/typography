const {runCommand} = require('./runCommand');

const getImageWidth = async path => {
    let imgWidth = 0;
    await runCommand({
        command: 'identify',
        options: [
            '-format',
            '\"%w\"',
            path
        ],
        onStdout: data => {
            imgWidth = parseInt(data.toString().replace(/"/ig, ''))
        },
    });
    return imgWidth
}

const getImageHeight = async path => {
    let imgHeight = 0;
    await runCommand({
        command: 'identify',
        options: [
            '-format',
            '\"%h\"',
            path
        ],
        onStdout: data => {
            imgHeight = parseInt(data.toString().replace(/"/ig, ''))
        },
    });
    return imgHeight
}

module.exports = {
    getImageHeight,
    getImageWidth
}
