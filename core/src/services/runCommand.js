const {spawn} = require('child_process');

async function runCommand({command, options = [], onStdout = () => {}, onStdErr = () => {}}) {
    if (!command) throw new Error('Command is required');
    return new Promise((resolve, reject) => {
        const process = spawn(command, options);
        process.stderr.on('data', data => {
            console.log(`stderr running command ${command} with options ${options}: ${data}`);
            onStdErr(data);
        });
        process.stdout.on('data', data => {
            console.log(`stdout running command ${command} with options ${options}: ${data}`);
            onStdout(data);
        });
        process.on('close', code => {
            console.log(`command ${command} with options ${options} exited with code ${code}`);
            if (!code) return resolve();
            reject();
        });
    });
}

module.exports = {
    runCommand
}
