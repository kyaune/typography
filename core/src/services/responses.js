const SuccessResponse = ({res, result}) => {
    return res.status(200).json({
        success: true,
        result,
        error: null
    });
}

const ErrorResponse = ({res, code = 500, error = {message: 'Internal error'}}) => {
    return res.status(code).json({
        success: false,
        error
    });
}

module.exports = {
    SuccessResponse,
    ErrorResponse,
}
