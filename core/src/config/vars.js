module.exports = {
    uploadsPath: process.env.UPLOADS_PATH || '/app/uploads',
    origin: process.env.ORIGIN || 'localhost'
};
