const {SuccessResponse, ErrorResponse} = require('../services/responses');
const {
    validateText,
    validateColorModel,
    validatePrintSize,
    validateImagesDPI,
    validateNumberOfPages
} = require('../services/templateValidation');
const {
    TEMPLATE_HEIGHT_SETS,
    TEMPLATE_SIZE_TO_WIDTH,
    TEMPLATE_NUMBER_OF_PAGES
} = require('../constants/TEMPLATE_PARAMS');
const {generatePreview} = require('../services/generatePreview');

const uploadTemplate = async (req, res) => {
    try {
        if (!req.file) return ErrorResponse({res, code: 400, error: {message: 'File is required'}});


        const isNumberOfPagesValid = await validateNumberOfPages(req.file.path, TEMPLATE_NUMBER_OF_PAGES[req.body.size]);
        if (!isNumberOfPagesValid) throw new Error('Неверное число страниц');
        const result = {
            isTextValid: await validateText(req.file.path),
            isColorModelValid: await validateColorModel(req.file.path),
            isPrintSizeValid: await validatePrintSize(
                req.file.path,
                TEMPLATE_SIZE_TO_WIDTH[req.body.size],
                TEMPLATE_HEIGHT_SETS[req.body.size]
            ),
            isImagesDPIValid: await validateImagesDPI(req.file.path)
        }
        const shouldGeneratePreview = Object.keys(result)
            .filter(key => key !== 'isImagesDPIValid')
            .every(key => result[key]);
        if (shouldGeneratePreview) {
            result.preview = await generatePreview(req.file.path, req.body.size);
        }
        return SuccessResponse({res, result});
    } catch (e) {
        console.log(e);
        return ErrorResponse({res, code: 500, error: {message: e.message}});
    }

}

module.exports = {
    uploadTemplate,
};
