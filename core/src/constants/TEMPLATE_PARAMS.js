const TEMPLATE_TYPES = {
    TRIO: 'trio',
    MONO: 'mono'
}

const TEMPLATE_WIDTHS = {
    '297':'297',
    '320':'320',
    '220':'220',
    '340':'340'
}

const TEMPLATE_HEIGHT_SETS = {
    [TEMPLATE_WIDTHS['297']]: [210, 200, 200, 200],
    [TEMPLATE_WIDTHS['320']]: [210, 200, 200, 200],
    [TEMPLATE_WIDTHS['220']]: [220, 280],
    [TEMPLATE_WIDTHS['340']]: [340, 280]
}

const reduceSum = [(acc, current) => acc + current, 0];
const TEMPLATE_TOTAL_HEIGHTS = Object.keys(TEMPLATE_HEIGHT_SETS).reduce((acc, current) => {
    acc[current] = TEMPLATE_HEIGHT_SETS[current].reduce(...reduceSum);
    return acc;
}, {})

const TEMPLATE_SIZE_TO_WIDTH = {
    [TEMPLATE_WIDTHS['297']]: 297,
    [TEMPLATE_WIDTHS['320']]: 320,
    [TEMPLATE_WIDTHS['220']]: 340,
    [TEMPLATE_WIDTHS['340']]: 340
}

const TEMPLATE_NUMBER_OF_PAGES = {
    [TEMPLATE_WIDTHS['297']]: 4,
    [TEMPLATE_WIDTHS['320']]: 4,
    [TEMPLATE_WIDTHS['220']]: 2,
    [TEMPLATE_WIDTHS['340']]: 2,
}

module.exports = {
    TEMPLATE_TYPES,
    TEMPLATE_HEIGHT_SETS,
    TEMPLATE_SIZE_TO_WIDTH,
    TEMPLATE_NUMBER_OF_PAGES,
    TEMPLATE_TOTAL_HEIGHTS
}
