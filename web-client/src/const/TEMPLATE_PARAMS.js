export const TEMPLATE_TYPES = {
    TRIO: 'trio',
    MONO: 'mono'
}

export const TEMPLATE_WIDTHS = {
    '297':'297',
    '320':'320',
    '220':'220',
    '340':'340'
}

export const TEMPLATE_HEIGHT_SETS = {
    [TEMPLATE_WIDTHS['297']]: [210, 200, 200, 200],
    [TEMPLATE_WIDTHS['320']]: [210, 200, 200, 200],
    [TEMPLATE_WIDTHS['220']]: [220, 280],
    [TEMPLATE_WIDTHS['340']]: [340, 280]
}

export const reduceSum = [(acc, current) => acc + current, 0];
export const TEMPLATE_TOTAL_HEIGHTS = Object.keys(TEMPLATE_HEIGHT_SETS).reduce((acc, current) => {
    acc[current] = TEMPLATE_HEIGHT_SETS[current].reduce(...reduceSum);
    return acc;
}, {})
