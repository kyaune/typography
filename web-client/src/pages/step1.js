import MainLayout from '../layout/MainLayout';
import TypeStage from '../components/TemplateForm/TypeStage';

export default function Home() {
    return (
        <MainLayout>
            <TypeStage />
        </MainLayout>
    )
}
