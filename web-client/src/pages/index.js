import MainLayout from '../layout/MainLayout';
import TemplateForm from '../components/TemplateForm';
import 'semantic-ui-css/semantic.min.css'

export default function Home() {
  return (
    <MainLayout>
      <TemplateForm />
    </MainLayout>
  )
}
