import React from 'react';

import styles from './MainLayout.module.scss';

const MainLayout = ({children}) => {

    return (
        <div className={styles.main}>
            <div className={styles.header}>
                Header
            </div>
            <div className={styles.content}>
                {children}
            </div>
            <div className={styles.footer}>
                Footer
            </div>
        </div>
    )
}

export default MainLayout;
