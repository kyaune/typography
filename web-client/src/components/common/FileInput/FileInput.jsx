import React, {forwardRef} from 'react';
import styles from './Fileinput.module.scss';

export default forwardRef(({onChange, inputProps}, ref) => {
    return <input ref={ref} type='file' className={styles.inputFile} onChange={onChange} multiple {...inputProps} />;
});
