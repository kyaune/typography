import React from 'react';
import ReactModal from 'react-modal';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './Modal.module.scss';

ReactModal.setAppElement('#__next');

const Modal = ({children, className, onClose}) => {
    ReactModal.setAppElement('body');
    return (
        <ReactModal
            onRequestClose={onClose}
            className={cn(styles.modal, className)}
            isOpen
            overlayClassName={styles.overlay}
        >
            {children}
        </ReactModal>
    )
}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    className: PropTypes.string,
    children: PropTypes.element.isRequired,
}

export default Modal;
