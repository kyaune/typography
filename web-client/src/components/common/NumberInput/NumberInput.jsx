import React, { useState } from 'react';
import style from './NumberInput.module.scss'

const NumberInput = () => {
    return (
        <div className={style.wrapper}>
        <div className={style.numberInput}>
            <input type="number" />
        </div>
        </div>
    )
}

export default NumberInput
