import React from 'react';
import Button from '../Button';

import styles from './ButtonSelect.module.scss';

const ButtonSelect = ({options, value, onChange}) => {
    return (
        <div className={styles.wrapper}>
            {options.map(option => (
                <Button onClick={() => onChange(option.value)} primary={option.value === value} key={option.value}>
                    {option.label}
                </Button>
            ))}
        </div>
    )
}

export default ButtonSelect;
