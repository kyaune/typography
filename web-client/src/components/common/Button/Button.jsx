import React, {useState} from 'react';
import cn from 'classnames';

import styles from './Button.module.scss';

const Button = ({children, className: externalClassName, primary, fontSize, onClick}) => {


    const [isHovering, setIsHovering] = useState(false)
    const handleMouseHover = () => {
        setIsHovering(!isHovering)
    }
    const className = cn(styles.wrapper, externalClassName, {
        [styles.primary]: primary,
    });
    const style = {
        fontSize,
    }
    return (
        <button className={className} style={style} onClick={onClick} onMouseEnter={handleMouseHover} onMouseLeave={handleMouseHover}>
            {children}
            {/*{*/}
            {/*    isHovering &&*/}
            {/*        <div>*/}
            {/*            Im a popup!*/}
            {/*        </div>*/}
            {/*}*/}
        </button>
    )
}

Button.defaultProps = {
    fontSize: '1rem'
}

export default Button;

