import React from 'react';
import cn from 'classnames';

import styles from './Text.module.scss';

const wrappers = {
    div: ({children, ...rest}) => <div {...rest}>{children}</div>,
    span: ({children, ...rest}) => <span {...rest}>{children}</span>
}

const Text = props => {
    const {
        children,
        primary,
        secondary,
        danger,
        highlight,
        className: externalClassName,
        align,
        span,
        div,
        ...rest
    } = props;

    const wrapperKey = Object.keys({span, div}).find(key => props[key]);
    const Wrapper = wrappers[wrapperKey];

    const className = cn(styles.wrapper, externalClassName, {
        [styles.primary]: primary,
        [styles.secondary]: secondary,
        [styles.danger]: danger,
        [styles.highlight]: highlight,
        [styles.center]: align === 'center',
        [styles.left]: align === 'left'
    });
    return (
        <Wrapper className={className} {...rest}>
            {children}
        </Wrapper>
    )
}

Text.defaultProps = {
    div: true,
}

export default Text;
