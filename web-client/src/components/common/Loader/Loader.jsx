import cn from 'classnames';
import React from 'react';
import styles from './Loader.module.scss';

const Loader = ({className}) => {
    return (
        <div className={cn(styles.loader, className)}>
            <svg className={styles.circular} viewBox="25 25 50 50">
                <circle className={styles.path} cx="50" cy="50" r="20" fill="none" strokeWidth="5"
                        strokeMiterlimit="10"/>
            </svg>
        </div>
    );
};

export default React.memo(Loader);
