import React from 'react';

const List = ({ol, children}) => {
    if (ol) return <ol>{children}</ol>;
    return <ul>{children}</ul>;
}

export default List;
