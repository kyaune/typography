import React from 'react';

import styles from './ProgressBar.module.scss';

const ProgressBar = ({progress, text}) => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.text}>{text}</div>
            <div className={styles.bar} style={{width: `${100 * progress}%`}}/>
        </div>
    )
}

export default ProgressBar;
