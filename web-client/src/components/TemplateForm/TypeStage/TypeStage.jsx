import React, {Fragment, useRef, useState} from 'react';
import {Popup} from 'semantic-ui-react'
import cn from 'classnames';

import Text from '../../common/Text';

import Button from '../../common/Button';
import List from '../../common/List';
import ButtonSelect from '../../common/ButtonSelect';
import NumberInput from '../../common/NumberInput'
import {TEMPLATE_HEIGHT_SETS, TEMPLATE_TOTAL_HEIGHTS, TEMPLATE_WIDTHS} from '../../../const/TEMPLATE_PARAMS';
import getConfig from 'next/config';
import styles from "./TypeStage.module.scss";

const typeOptions = [
    {value: TEMPLATE_WIDTHS['220'], label: 'Моно 220'},
    {value: TEMPLATE_WIDTHS['340'], label: 'Моно 340'},
    {value: TEMPLATE_WIDTHS['297'], label: 'Трио 297'},
    {value: TEMPLATE_WIDTHS['320'], label: 'Трио 320'},
]

//трио: серебристый с белым фоном и  серебристый с голубым фоном
// моно: выбирать не нужно - по дефолту с белым фоном
const numbOptions = [
    {value: 'Серебристый с белым фоном', label: 'Серебристый с белым фоном'},
    {value: 'Серебристый с голубым фоном', label: 'Серебристый с белым фоном'},
]

const cursorOptions = [
    {value: 'Квадратный на ленте', label: 'Квадратный на ленте'},
    {value: 'Статический круглый', label: 'Статический круглый'},
]

const luvColorOptions = [
    {value: 'Золото', label: 'Золото'},
    {value: 'Серебро', label: 'Серебро'},
]

const springColorOptions = [
    {value: 'Белая', label: 'Белая'},
    {value: 'Черная', label: 'Черная'},
]

const headerOptions = [
    {value: 'Обычный картон', label: 'Обычный картон'},
    {value: 'Матовая ламинация', label: 'Матовая ламинация'},
    {value: 'Глянцевая ламинация', label: 'Глянцевая ламинация'},
]
//если обычный картон, матовая ламинация , то добавляется параметр - Выборочный лак:
const varnishOptions = [
    {value: 'Не нужен', label: 'Не нужен'},
    {value: 'Нужен', label: 'Нужен'},
]

const foilOptions = [
    {value: 'Не нужна', label: 'Не нужна'},
    {value: '3D золото', label: '3D золото'},
    {value: '3D голография', label: '3D голография'},
    {value: '3D бронза', label: '3D бронза'},
    {value: '3D красный', label: '3D красный'},
    {value: '3D малиновый', label: '3D синий'},
    {value: '3D зеленый', label: '3D зеленый'},
]

const packagingOptions = [
    {value: 'Упаковка в пакеты не нужна', label: 'Упаковка в пакеты не нужна'},
    {value: 'Упаковка в индивидуальные пакеты', label: 'Упаковка в индивидуальные пакеты'},
]

const circulationOptions = [
    {value: 10, label: '10'},
    {value: 20, label: '20'},
    {value: 30, label: '30'},
    {value: 50, label: '50'},
    {value: 100, label: '100'},
    {value: 200, label: '200'},
]

const TypeStage =() => {

    const [type, setType] = useState('')
    const [numb, setNumb] = useState('')
    const [cursor, setCursor] = useState('')
    const [luvColor, setLuvColor] = useState('')
    const [springColor, setSpringColor] = useState('')
    const [header, setHeader] = useState('')
    const [varnish, setVarnish] = useState('')
    const [foil, setFoil] = useState('')
    const [packaging, setPackaging] = useState('')
    const [circulation, setCirculation] = useState(null)

    const handleTypeChange = type => {
        setType(type);
    }

    return (
        <div className={styles.wrapper}>
            <ul>
                <li>
                    <label>Вида календаря</label>
                    <div className={styles.buttonGroup}>
                    <ButtonSelect options={typeOptions} value={type} onChange={(e) => setType(e)}/>
                    </div>
                </li>
                    <li>
                    <label>Численники</label>
                    <div className={styles.buttonGroup}>
                        <ButtonSelect options={numbOptions} value={numb} onChange={(e) => setNumb(e)}/>
                    </div>
                </li>
                {  numb === '' &&
                <li>
                    <label className={styles.adviceText}>Подсказка</label>
                    <div className={styles.buttonGroup}>
                    </div>
                </li>
                }
                {
                    numb.length > 0 &&
                    <li>
                    <label>Курсор</label>
                    <div className={styles.buttonGroup}>
                        <ButtonSelect options={cursorOptions} value={cursor} onChange={(e) => setCursor(e)}/>
                    </div>
                </li>
                }
                { cursor.length > 0 &&
                    <li>
                        <label>Цвет люверса</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={luvColorOptions} value={luvColor} onChange={(e) => setLuvColor(e)}/>
                        </div>
                    </li>
                }
                { luvColor.length > 0 &&
                    <li>
                        <label>Цвет пружинки</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={springColorOptions} value={springColor}
                                          onChange={(e) => setSpringColor(e)}/>
                        </div>
                    </li>
                }
                { springColor.length > 0 &&
                    <li>
                        <label>Обработка шапки</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={headerOptions} value={header} onChange={(e) => setHeader(e)}/>
                        </div>
                    </li>
                }
                {   (header === headerOptions[0].value || header === headerOptions[1].value)  &&
                    <li>
                        <label>Выборочный лак</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={varnishOptions} value={varnish} onChange={(e) => setVarnish(e)}/>
                        </div>
                    </li>
                }
                { (varnish.length > 0 || header === 'Глянцевая ламинация') &&
                    <li>
                        <label>Фольга</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={foilOptions} value={foil} onChange={(e) => setFoil(e)}/>
                        </div>
                    </li>
                }
                { foil.length > 0 &&
                    <li>
                        <label>Упаковка календарей в пакеты</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={packagingOptions} value={packaging}
                                          onChange={(e) => setPackaging(e)}/>
                        </div>
                    </li>
                }
                { packaging.length > 0 &&
                    <li>
                        <label>Тираж</label>
                        <div className={styles.buttonGroup}>
                            <ButtonSelect options={circulationOptions} value={circulation}
                                          onChange={(e) => setCirculation(e)}>
                                </ButtonSelect>
                        </div>
                    </li>
                }
            </ul>
            { circulation > 0 &&
               <Button>Далее</Button>
            }
        </div>
    )
}

export default TypeStage;
