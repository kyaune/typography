import React from 'react';
import UploadStage from './UploadStage';

import styles from './TemplateForm.module.scss';

const TemplateForm = () => {

    return (
        <div className={styles.wrapper}>
            <UploadStage />
        </div>
    )
}

export default TemplateForm;
