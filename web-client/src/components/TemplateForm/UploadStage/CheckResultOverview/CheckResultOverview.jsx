import React, {Fragment} from 'react';
import Text from '../../../common/Text';

const RESULT_CATEGORIES = {
    isTextValid: 'Шрифты в кривых',
    isPrintSizeValid: 'Вылеты',
    isImagesDPIValid: 'Разрешение изображений',
    isColorModelValid: 'Цветовая модель'
}

const CheckResultOverview = ({checkResult}) => {
    return (
        <Fragment>
            <br />
            <Text>Документ проверен</Text>
            {Object.keys(checkResult).filter(key => key !== 'preview').map(key => (
                <Text key={key}>
                    {RESULT_CATEGORIES[key]}
                    {' '}
                    -
                    {' '}
                    <Text span {...{[checkResult[key] ? 'highlight': 'danger']: true}}>
                        {checkResult[key] ? 'ок' : 'проверка не пройдена'}
                    </Text>
                </Text>
            ))}
        </Fragment>
    )
}

export default CheckResultOverview;
