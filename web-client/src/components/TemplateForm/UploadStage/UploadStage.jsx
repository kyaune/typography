import React, {Fragment, useRef, useState} from 'react';
import cn from 'classnames';

import Text from '../../common/Text';

import styles from './UploadStage.module.scss';
import Button from '../../common/Button';
import List from '../../common/List';
import FileInput from '../../common/FileInput/FileInput';
import uploadFile from '../../../utils/uploadFiles';
import Modal from '../../common/Modal';
import ButtonSelect from '../../common/ButtonSelect';
import {TEMPLATE_HEIGHT_SETS, TEMPLATE_TOTAL_HEIGHTS, TEMPLATE_WIDTHS} from '../../../const/TEMPLATE_PARAMS';
import getConfig from 'next/config';
import ProgressBar from '../../common/ProgressBar';
import CheckResultOverview from './CheckResultOverview';
import PreUploadHints from './PreUploadHints';
import PostUploadErrorHints from './PostUploadErrorHints';

const ERROR_MESSAGES = {
    isTextValid: 'В прикрепленном макете присутствует текст не в кривых',
    isPrintSizeValid: 'Неправильный размер листов макета',
    isImagesDPIValid: 'Низкое разрешение картинок',
    isColorModelValid: 'Присутствуют страницы в RGB'
}

const selectOptions = [
    {value: TEMPLATE_WIDTHS['220'], label: 'Моно 220'},
    {value: TEMPLATE_WIDTHS['340'], label: 'Моно 340'},
    {value: TEMPLATE_WIDTHS['297'], label: 'Трио 297'},
    {value: TEMPLATE_WIDTHS['320'], label: 'Трио 320'},
]

const UploadStage = () => {
    const fileInputRef = useRef();
    const [currentFile, setFile] = useState(null);
    const [showErrorModal, setShowErrorModal] = useState(null);
    const [showMistakesModal, setShowMistakesModal] = useState(false);
    const [checkResult, setCheckResult] = useState(null);
    const [isProcessing, setIsProcessing] = useState(false);
    const [type, setType] = useState(TEMPLATE_WIDTHS['220'])
    const [preview, setPreview] = useState('');
    const [progress, setProgress] = useState(0);

    const handleFileUpload = async e => {
        const file = e.target.files[0];
        if (!file) return;
        setFile(file);
        const data = new FormData();
        data.append('template', file);
        data.append('size', type);
        const path = 'templates/upload-template';
        try {
            setIsProcessing(true);
            setTimeout(() => {
                setProgress(() => 0.95)
            }, 1000);
            const {result} = await uploadFile({data, path});
            setIsProcessing(false);
            setCheckResult(result);
            setPreview(result.preview);
        } catch (e) {
            setShowErrorModal(e.message);
            setIsProcessing(false);
        }
    }
    const onFileRemove = () => {
        setFile(null);
        setCheckResult(null);
        setPreview('');
        setProgress(0);
    }
    const handleTypeChange = type => {
        setType(type);
        onFileRemove();
    }

    return (
        <div className={styles.wrapper}>
            <div>
                <ButtonSelect options={selectOptions} value={type} onChange={handleTypeChange}/>
                <br />
                <Button primary fontSize='2rem' onClick={() => fileInputRef.current.click()}>
                    Добавить файл
                </Button>
                <br />
                <div className={styles.row}>
                    <Text primary>
                        Загруженные файлы: {!!currentFile ? 1 : 0}
                    </Text>
                    {isProcessing && <ProgressBar text='Проверка' progress={progress}/>}
                </div>
                {!!currentFile && (
                    <Fragment>
                        <br/>
                        <div className={styles.currentFileContainer}>
                            <div className={styles.name}>{currentFile.name}</div>
                            <div className={styles.cross} onClick={onFileRemove}>+</div>
                        </div>
                        {checkResult && <CheckResultOverview checkResult={checkResult}/>}
                    </Fragment>
                )}
                <br />
                <PreUploadHints />
            </div>
            <div className={styles.previewCol}>
                {checkResult && <PostUploadErrorHints checkResult={checkResult}/>}
                {preview && (
                    <Fragment>
                        <div className={styles.previewWrapper}>
                            <img src={`${getConfig().publicRuntimeConfig.publicUrl}/${preview}`}/>
                            <div className={styles.legendContainer}>
                                <Text danger className={cn(styles.underline, styles.danger)}>Линия отступа</Text>
                                <Text
                                    className={cn(styles.underline, styles.shadeZone)}
                                    style={{
                                        top: `${
                                            (TEMPLATE_HEIGHT_SETS[type][0] + TEMPLATE_HEIGHT_SETS[type][1] - 50) * 100 /
                                            TEMPLATE_TOTAL_HEIGHTS[type]
                                        }%`,
                                    }}
                                >
                                    Зона тени
                                </Text>
                            </div>
                        </div>
                    </Fragment>
                )}
            </div>
            <FileInput
                ref={fileInputRef}
                onChange={handleFileUpload}
                inputProps={{
                    accept: 'application/pdf,application/postscript'
                }}
            />
            {!!showMistakesModal && (
                <Modal onClose={() => setShowMistakesModal(null)}>
                    <List ul>
                        {Object.keys(showMistakesModal)
                            .filter(key => !showMistakesModal[key])
                            .map(key => <li key={key}>{ERROR_MESSAGES[key]}</li>)
                        }
                    </List>
                    <Button primary onClick={() => setShowMistakesModal(false)}>
                        Закрыть
                    </Button>
                </Modal>
            )}
            {showErrorModal && (
                <Modal onClose={() => setShowErrorModal(false)}>
                    <div className={styles.errorText}>{showErrorModal}</div>
                    <Button primary onClick={() => setShowErrorModal(false)}>
                        Закрыть
                    </Button>
                </Modal>
            )}
        </div>
    )
}

export default UploadStage;
