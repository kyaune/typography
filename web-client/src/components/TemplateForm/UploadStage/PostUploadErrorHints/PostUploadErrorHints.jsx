import React, {Fragment} from 'react';
import List from '../../../common/List';

const AHref = props => <a href='https://www.pomidorprint.ru/' target='_blank'>{props.children}</a>
const RESULT_HINTS = {
    isTextValid: () => (
        <>
            Шрифты не в кривых. Вернитесь к файлу и переведите шрифты в кривые.
            Даже если у вас pdf. Это нужно, чтобы текст отображался корректно.
            {' '}
            <AHref>Подсказка</AHref>
        </>
    ),
    isImagesDPIValid: () => (
        <>
            Низкое разрешение картинки.
            Мы рекомендуем ставить разрешение для печати 300dpi.
            Если у вас ниже, то печать может выйти пиксельной и не качественной.
            {' '}
            <AHref>Подсказка</AHref>
        </>
    ),
    isColorModelValid: () => (
        <>
            Ошибка в цветовой профиле. Проверьте, что сохранили файл в CMYK!
            {' '}
            <AHref>Подсказка</AHref>
        </>
    ),
    isPrintSizeValid: () => (
        <>
            Ошибка в вылетах или размерах.
            Проверьте, что размер в макете стоит верный и включает в себя вылеты по 2 мм с каждой стороны.
            {' '}
            <AHref>Подсказка</AHref>
        </>
    ),
}

const PostUploadErrorHints = ({checkResult}) => {
    const keys = Object.keys(checkResult).filter(k => k !== 'preview' && !checkResult[k]);

    return (
        <List ul>
            {keys.map(k => (
                <li key={k}>
                    {RESULT_HINTS[k]()}
                </li>
            ))}
            {!!keys.length && (
                <Fragment>
                    <li>
                        Рекомендуем ознакомится с техническими требованиями:
                        <br />
                        <a href='https://l.pomidorprint.ru/tech' target='_blank'>Технические требования</a>
                    </li>
                    <li>
                        <AHref>Скачать</AHref> шаблон для календаря
                        <br />
                        Возникли сложности? Напиши нам
                        {' '}
                        <a href='mailto:ipomidor@pomidorprint.ru'>ipomidor@pomidorprint.ru</a>
                    </li>
                </Fragment>
            )}
        </List>
    )
}

export default PostUploadErrorHints;
