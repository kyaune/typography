const express = require('express');
const next = require('next');
const compression = require('compression');

const apiPort = parseInt(process.env.DEV_API_PORT, 10) || 8000;
const apiHost = process.env.DEV_API_HOST || 'localhost';
const apiProtocol = process.env.DEV_API_PROTOCOL || 'http';
const port = parseInt(process.env.PORT, 10) || 3000;

const env = process.env.NODE_ENV;
const dev = env !== 'production';

const devProxy = {
    '/api': {
        target: `${apiProtocol}://${apiHost}:${apiPort}/api/`,
        pathRewrite: { '^/api': '/' },
        changeOrigin: true,
    },
};

const app = next({
    dev,
});

const handle = app.getRequestHandler();
app.prepare()
    .then(() => {
        const server = express();
        //server.use(nextI18NextMiddleware(nextI18next))

        if (!dev) {
            server.use(compression());
        }

        // Set up the proxy.
        if (dev && devProxy) {
            const proxyMiddleware = require('http-proxy-middleware');

            Object.keys(devProxy).map(context => {
                server.use(proxyMiddleware(context, devProxy[context]));
            });
        }

        // Default catch-all handler to allow Next.js to handle all other routes
        server.all('*', (req, res) => handle(req, res));

        server.listen(port, err => {
            if (err) {
                throw err;
            }

            console.log(`> Ready on port ${port} [${env}]`);
        })
    })
    .catch(err => {
        console.log('An error occurred, unable to start the server');
        console.log('Error; server.js;', err);
    });
