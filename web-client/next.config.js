const publicRuntimeConfig = {
    apiUrl: process.env.API_URL || 'http://localhost:3000/api',
    publicUrl: process.env.PUBLIC_URL || 'http://localhost:8000'
};

module.exports = {
    publicRuntimeConfig,
    distDir: '.next',
    dir: './src',

    webpackDevMiddleware: config => {
        config.watchOptions = {
            poll: 1000,
            aggregateTimeout: 300,
        }

        return config
    },
};
